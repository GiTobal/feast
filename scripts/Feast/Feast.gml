// Feather ignore all

enum feastType 
{
	square,
	circle
}

/// @param {real} x
/// @param {real} y
/// @param {real} width
/// @param {real} height
/// @param {Id.Instance} instance
/// @param {array}       targets
function Feast(_x, _y, _w, _h, _ins = other, _targets=[]) constructor 
{
	id  = _ins;
	
	// A que objeto afecta la colision
	targets = _targets;
	
	// Posicion
	x = id.x + _x;
	y = id.y + _y;
	
	xoffset = id.sprite_xoffset;
	yoffset = id.sprite_yoffset;
	
	xreal = _x;
	yreal = _y;
	
	xscale = id.image_xscale;
	yscale = id.image_yscale;
	
	// Tamaño
	w = _w;
	h = _h;
	
	type = feastType.square;
	// Si se utiliza una forma de circulo
	rad  = radioSquare();
	
	col = c_red;
	
	// Tiempos
	timeAppear = 0;
	timeAppearRes = timeAppear;
	
	timeActive = 0;
	timeActiveRes = timeActive;
	
	visible = false;
	always  = false;
	
	// Estadisticas
	str  = 0; // Daño causado
	push = 0; // Fuerza de impulso
	ang  = 1; // Direccion a la que manda el golpe
	
	hitstun = 0; // Tiempo en que coloca al objetivo en hitstun 
	
	fnContact = function(_this, _other) {
		return true; 
	}
	
	#region METHODS
	
	/// @ignore
	static radioSquare = function() 
	{
		return sqrt(sqr(w * xscale) + sqr(h * yscale));
	}
	
	
	static updateVars = function() 
	{
		xoffset = id.sprite_xoffset;
		yoffset = id.sprite_yoffset;
		
		xscale = id.image_xscale;
		yscale = id.image_yscale;
		
		return self;
	}

	static attach = function() 
	{
		updateVars();
		
		x = round(id.x + (xreal + xoffset) * xscale );
		y = round(id.y + (yreal + yoffset) * yscale );
	}
	
	/// @param {real} strength
	/// @param {real} pushForce
	/// @param {real} pushAngle
	/// @param {real} hitstun
	static setAttackVars = function(_str, _push, _ang, _hit) 
	{
		str = _str;
		
		push = _push;
		ang  = _ang ;
		
		hitstun = _hit;
		
		return self;
	}        
	
	/// @param {real} appearTime
	/// @param {real} activeTime
	static setTimers = function(_appear, _active) 
	{
		timeActive = _active;
		timeAppear = _appear;
		
		// Valores para resetear el timer
		timeActiveRes = timeActive;
		timeAppearRes = timeAppear;
		
		return self;
	}
	
	/// @param {Id.Instance} instances
	static addTargets = function(_ins) 
	{
		array_push(targets, _ins);
		return self;
	}
	
	static targetGet  = function() 
	{
		return targets;
	}
	
	
	static getCollision = function() 
	{
		static list = ds_list_create();
		var _numb = 0;
		
		switch (type) {
			case feastType.square: 
				_numb = collision_rectangle_list(x, y, x + (w * xscale), y + (h * yscale), all, false, true, list, false); 
			break;
			
			case feastType.circle: 
				_numb = collision_circle_list(x, y, rad, all, false, true, list, false);
			break;
		}
		
		return [list, _numb];
	}
	
	static drawCollision = function(_color, _alpha) 
	{
		draw_set_alpha(_alpha)
		draw_set_color(_color)
		switch (type) {
			case feastType.square: draw_rectangle(x, y, x + w * xscale, y + h * yscale, false); break;
			case feastType.circle: draw_circle   (x, y, rad, false);  break;
		}
		draw_set_color(c_white);
		draw_set_alpha(1);
	}

	
	static check = function() 
	{
		// Si no es visible o no tiene objetivos entonces no comprobar nada
		if (!visible) && (is_undefined(obj) ) exit;
	
		// Actualizar posicion.
		updateVars();
		
		var _collision = getCollision();
		
		var _list = _collision[0];
		var _size = _collision[1];
		
		// -- Recorrer lista
		for (var i=0; i < _size; ++i) {
			var _ins = _list[| i];
			
			if (_ins != noone) {
				if (fnContact(id, _ins) ) {
					visible = false;
				} else {
					visible = true ;
				}
			}
		}
		
		// Limpiar lista
		ds_list_clear(_list);
	}
	
	static draw = function(_color, _alpha, _bool = true) 
	{
		if (_bool) {
			if (visible) drawCollision(_color, _alpha);
		
		} else drawCollision(_color, _alpha);
	}
	
	static setFnContact  = function(_fn) 
	{
		static fn = function() {return true};
		if (!is_undefined(_fn) ) {
			fnContact = method(id, _fn);
		} else {
			fnContact = fn;
		}
		return self;
	}
	
	/// @param {bool} resetTimer
	static runAppearTime = function(_reset = false) {
		if (timeAppear >= 0) {
			timeAppear--; 
		} else {
			visible = true;  
			if (_reset) timeAppear = timeAppearRes;
		}
	}
	
	/// @param {bool} resetTimer
	static runActiveTime = function(_reset = false) {
		if (timeActive >= 0) {
			timeActive--;
		} else {
			visible = false; 
			if (_reset) timeActive = timeActiveRes;
		}
	}
	
	#endregion
}

